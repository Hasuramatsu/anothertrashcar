// Copyright Epic Games, Inc. All Rights Reserved.

#include "TrashCar.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TrashCar, "TrashCar" );

DEFINE_LOG_CATEGORY(LogTrashCar)
 