// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TrashCarGameMode.generated.h"

UCLASS(minimalapi)
class ATrashCarGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATrashCarGameMode();
};



