// Copyright Epic Games, Inc. All Rights Reserved.

#include "TrashCarGameMode.h"
#include "TrashCarPlayerController.h"
#include "UObject/ConstructorHelpers.h"

ATrashCarGameMode::ATrashCarGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATrashCarPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	//if (PlayerPawnBPClass.Class != NULL)
	//{
	//	DefaultPawnClass = PlayerPawnBPClass.Class;
	//}
}